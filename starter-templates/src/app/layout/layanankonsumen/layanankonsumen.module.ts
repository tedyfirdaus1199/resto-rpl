import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LayanankonsumenComponent } from './layanankonsumen.component';
import { ReadmailComponent } from './readmail/readmail.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: LayanankonsumenComponent },
            { path: 'readmail', component: ReadmailComponent },
        ]),
    ],
    declarations: [

        LayanankonsumenComponent,

        ReadmailComponent,

    ],
})
export class LayanankonsumenModule { }

