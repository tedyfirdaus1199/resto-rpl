import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LayanankonsumenComponent } from './layanankonsumen.component';

describe('LayanankonsumenComponent', () => {
  let component: LayanankonsumenComponent;
  let fixture: ComponentFixture<LayanankonsumenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LayanankonsumenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LayanankonsumenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
