import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PembayaranComponent } from './pembayaran.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: PembayaranComponent },
        ]),
    ],
    declarations: [

        PembayaranComponent,

    ],
})
export class PembayaranModule { }
