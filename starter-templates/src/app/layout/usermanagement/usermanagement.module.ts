import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { UsermanagementComponent } from './usermanagement.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: UsermanagementComponent },
        ]),
    ],
    declarations: [

        UsermanagementComponent,

    ],
})
export class UserModule { }
