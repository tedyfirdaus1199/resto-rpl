import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UsermanagementService } from '../../services/usermanagement.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-usermanagement',
  templateUrl: './usermanagement.component.html',
  styleUrls: ['../../../styles.scss']
})
export class UsermanagementComponent implements OnInit {

users: Observable<any>;

  constructor(private usermanagement: UsermanagementService) { }

  ngOnInit() {
  	this.users = this.usermanagement.getuser(null);
  }
}
