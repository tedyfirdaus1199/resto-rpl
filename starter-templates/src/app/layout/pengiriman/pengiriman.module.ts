import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PengirimanComponent } from './pengiriman.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: PengirimanComponent },
        ]),
    ],
    declarations: [

        PengirimanComponent,


    ],
})
export class PengirimanModule { }
