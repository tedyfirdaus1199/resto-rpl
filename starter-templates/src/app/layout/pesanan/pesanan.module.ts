import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PesananComponent } from './pesanan.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([

            { path: '', component: PesananComponent },
        ]),
    ],
    declarations: [

        PesananComponent,

    ],
})
export class PesananModule { }
