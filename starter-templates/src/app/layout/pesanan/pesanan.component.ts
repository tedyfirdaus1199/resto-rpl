import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PesananService } from '../../services/pesanan.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-pesanan',
  templateUrl: './pesanan.component.html',
  styleUrls: ['../../../styles.scss']
})
// export class PesananComponent implements OnInit {

//   constructor(private pesanan: PesananService) { }

//   ngOnInit() {
//   	this.pesanan.getPesanan().subscribe(pesanan => {
//   		console.log(pesanan);
//   	},
//   	error=>{
//   		console.log(error);
//   	}
//   	);
//   }

//   getPesanan() {
//   	this.pesanan.getPesanan().subscribe(pesanan=> {
//   		console.log(pesanan);
//   	},
//   	error=>{
//   		console.log(error);
//   	}
//   	);
//   }

// }

export class PesananComponent {
  
	pesanans: Observable<any>;
	kode_pesanan = '';
    kode_user = '';
    kode_makanan = '';
    jumlah_makanan = '';
    kode_minuman = '';
    jumlah_minuman = '';
    subtotal_makanan = '';
    subtotal_minuman = '';
    total_bayar = '';
    kode_promo = '';
    meja = '';
  
	constructor(private router: Router, private pesanan: PesananService) { }
  
	ngOnInit() {
		this.pesanans = this.pesanan.getPesanan(null);
	}

	tambahpesanan(){
		this.pesanan.tambahdata(this.kode_pesanan, this.kode_user, this.kode_makanan, this.jumlah_makanan, this.kode_minuman, this.jumlah_minuman, this.subtotal_makanan, this.subtotal_minuman, this.total_bayar, this.kode_promo, this.meja)
	}
}