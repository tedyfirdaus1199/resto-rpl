import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PromoService } from '../../services/promo.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-promo',
  templateUrl: './promo.component.html',
  styleUrls: ['../../../styles.scss']
})
export class PromoComponent implements OnInit {
  
  promos: Observable<any>;
  nama_promo = '';
  kd_promo	= '';
  kategori	= '';
  jumlah_potongan	= '';
  max_penggunaan	= '';
  tgl_selesai	= '';
  keterangan	= '';
  s_dan_k	= '';


  constructor(
  private router: Router,
  private promo: PromoService) { }

  ngOnInit() {
  	this.promos = this.promo.getPromo(null);
  }

  tambahpromo() {
  	this.promo.tambahdata(this.nama_promo, this.kd_promo, this.kategori, this.jumlah_potongan, this.max_penggunaan, this.tgl_selesai, this.keterangan, this.s_dan_k)
  }
}
