import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { PromoComponent } from './promo.component';
import { KelolaPromoComponent } from './kelola-promo/kelola-promo.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        RouterModule.forChild([

            { path: '', component: PromoComponent },
            { path: 'kelolapromo', component: KelolaPromoComponent }
        ]),
    ],
    declarations: [

        PromoComponent,
        KelolaPromoComponent,

    ],
})
export class PromoModule { }
