import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KelolaPromoComponent } from './kelola-promo.component';

describe('KelolaPromoComponent', () => {
  let component: KelolaPromoComponent;
  let fixture: ComponentFixture<KelolaPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KelolaPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KelolaPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
