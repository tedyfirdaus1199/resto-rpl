import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { LaporanComponent } from './laporan.component';
import { LaporanHotMenuComponent } from './laporan-hot-menu/laporan-hot-menu.component';
import { LaporanOrderanComponent } from './laporan-orderan/laporan-orderan.component';
import { AktivitasPegawaiComponent } from './aktivitas-pegawai/aktivitas-pegawai.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: LaporanComponent },
            { path: 'laporan-hot-menu', component: LaporanHotMenuComponent },
            { path: 'laporan-orderan', component: LaporanOrderanComponent },
            { path: 'aktivitas-pegawai', component: AktivitasPegawaiComponent }
        ]),
    ],
    declarations: [

        LaporanComponent,

        LaporanHotMenuComponent,

        LaporanOrderanComponent,

        AktivitasPegawaiComponent

    ],
})
export class LaporanModule { }
