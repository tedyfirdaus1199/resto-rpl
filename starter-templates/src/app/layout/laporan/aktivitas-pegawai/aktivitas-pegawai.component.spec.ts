import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AktivitasPegawaiComponent } from './aktivitas-pegawai.component';

describe('AktivitasPegawaiComponent', () => {
  let component: AktivitasPegawaiComponent;
  let fixture: ComponentFixture<AktivitasPegawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AktivitasPegawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AktivitasPegawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
