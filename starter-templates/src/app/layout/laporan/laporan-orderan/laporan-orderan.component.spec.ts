import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanOrderanComponent } from './laporan-orderan.component';

describe('LaporanOrderanComponent', () => {
  let component: LaporanOrderanComponent;
  let fixture: ComponentFixture<LaporanOrderanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanOrderanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanOrderanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
