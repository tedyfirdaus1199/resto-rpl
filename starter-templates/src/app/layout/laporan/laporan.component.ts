import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import * as CanvasJS from './canvasjs.min';
//var CanvasJS = require('./canvasjs.min');

@Component({
  selector: 'app-laporan',
  templateUrl: './laporan.component.html',
  styleUrls: ['../../../styles.scss']
})
export class LaporanComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    let chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      exportEnabled: false,
      title: {
        text: " "
      },
      data: [{
        type: "line",
        dataPoints: [
          { y: 300000, label: "Senin" },
          { y: 205000, label: "Selasa" },
          { y: 190000, label: "Rabu" },
          { y: 210000, label: "Kamis" },
          { y: 321000, label: "Jum'at" },
          { y: 150000, label: "Sabtu" },
          { y: 125000, label: "Minggu" }
        ]
      }]
    });
      
    chart.render();
  }

}
