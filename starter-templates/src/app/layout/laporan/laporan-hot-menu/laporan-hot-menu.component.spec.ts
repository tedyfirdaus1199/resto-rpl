import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaporanHotMenuComponent } from './laporan-hot-menu.component';

describe('LaporanHotMenuComponent', () => {
  let component: LaporanHotMenuComponent;
  let fixture: ComponentFixture<LaporanHotMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaporanHotMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaporanHotMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
