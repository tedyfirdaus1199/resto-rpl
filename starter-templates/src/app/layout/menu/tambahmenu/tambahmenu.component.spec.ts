import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TambahmenuComponent } from './tambahmenu.component';

describe('TambahmenuComponent', () => {
  let component: TambahmenuComponent;
  let fixture: ComponentFixture<TambahmenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TambahmenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TambahmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
