import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MenuComponent } from './menu.component';
import { TambahmenuComponent } from './tambahmenu/tambahmenu.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: MenuComponent },
            { path: 'tambahmenu', component: TambahmenuComponent },
        ]),
    ],
    declarations: [

        MenuComponent,

        TambahmenuComponent,

    ],
})
export class MenuModule { }
