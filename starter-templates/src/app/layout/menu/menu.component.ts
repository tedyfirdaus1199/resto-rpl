import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['../../../styles.scss']
})
export class MenuComponent implements OnInit {
  modalService: any;

  constructor() { }


 ngOnInit() {
  	this.getData();
  }

  dataSource:any=[];
  user:any={};

  getData()
  {
  		this.dataSource=[
  			{
  				nama: 'Sambal Dabu - Dabu',
          keterangan:'Resep Sambal',
          jumlah:'100',
          harga:'20500'
  			},
  			{
  				nama: 'Sambal Mangga Muda',
          keterangan:'Resep Sambal',
          jumlah:'100',
          harga:'20500'
  			},
  			{
  				nama: 'Sambal Petai',
          keterangan:'Resep Sambal',
          jumlah:'100',
          harga:'20500'
        },
        {
  				nama: 'Sambal Petai',
          keterangan:'Resep Sambal',
          jumlah:'100',
          harga:'20500'
  			},


  		];
  }


  open(content) {
    this.modalService.open(content);
    console.log(content);
  }


  saveData(user)
  {
  	console.log(user);
  	this.dataSource.unshift(user);
  	//this.user={};
  }

  deleteData(idx)
  {
  	var konfirmasi=confirm('Yakin akan menghapus data?');
  	if(konfirmasi==true)
  	{
  		this.dataSource.splice(idx,1);
  	}
  }


}

