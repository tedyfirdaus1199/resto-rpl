import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $;
@Component({
  selector: 'app-verifikasi',
  templateUrl: './verifikasi.component.html',
  styleUrls: ['../../../styles.scss']
})
export class VerifikasiComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    document.body.className = 'hold-transition verifikasi-page';
    $(() => {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  }

  login() {
    this.router.navigate(['login']);
  }
}
