import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/*Custom Files*/
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { LupasandiComponent } from './lupasandi/lupasandi.component';
import { VerifikasiComponent } from './verifikasi/verifikasi.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../services/auth.service';

const routes: Routes = [
	{
		path: '',
		component: LoginComponent
	}
];

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
	FormsModule,
	ReactiveFormsModule,
	RouterModule.forChild(routes)
  ],
	exports: [RouterModule],
	providers: [AuthService],

  declarations: [LoginComponent, RegisterComponent, LupasandiComponent, VerifikasiComponent]
})
export class LoginModule { }
