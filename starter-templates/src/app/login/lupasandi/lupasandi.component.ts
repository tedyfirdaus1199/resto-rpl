import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

declare var $;
@Component({
  selector: 'app-lupasandi',
  templateUrl: './lupasandi.component.html',
  styleUrls: ['../../../styles.scss']
})
export class LupasandiComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    document.body.className = 'hold-transition lupasandi-page';
    $(() => {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  }

  lupasandi() {
    this.router.navigate(['login']);
  }

  verifikasi() {
    this.router.navigate(['verifikasi']);
  }

}
