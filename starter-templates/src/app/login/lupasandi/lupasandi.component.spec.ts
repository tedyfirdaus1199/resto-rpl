import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LupasandiComponent } from './lupasandi.component';

describe('LupasandiComponent', () => {
  let component: LupasandiComponent;
  let fixture: ComponentFixture<LupasandiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LupasandiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LupasandiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
