import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { KatalogComponent } from './katalog/katalog.component';
import { PublicComponent } from './public.component';


const routes: Routes = [
    {
        path: '', component: PublicComponent,

    }
];
@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PublicRoutingModule { }
