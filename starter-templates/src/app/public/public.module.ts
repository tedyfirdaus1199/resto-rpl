import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PublicComponent } from './public.component';
import { LandingComponent } from './landing/landing.component';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { FooterComponent } from '../shared/footer/footer.component';
import { KatalogComponent } from './katalog/katalog.component';
import { PembayaranComponent } from './pembayaran/pembayaran.component';
import { PesananComponent } from './pesanan/pesanan.component';
import { PengirimanComponent } from './pengiriman/pengiriman.component';
import { PromosiComponent } from './promosi/promosi.component';




@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: PublicComponent },
        ]),
    ],
    declarations: [
        PublicComponent,
        LandingComponent,
        NavbarComponent,
        FooterComponent,
        PembayaranComponent,
        PesananComponent,
        PengirimanComponent,
        PromosiComponent,


    ],
    providers: [],
    bootstrap: [PublicComponent]
})
export class PublicModule { }
