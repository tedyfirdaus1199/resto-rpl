import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { KatalogComponent } from './katalog.component';



@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: KatalogComponent },
        ]),
    ],
    declarations: [

        KatalogComponent,


    ],
})
export class KatalogModule { }
