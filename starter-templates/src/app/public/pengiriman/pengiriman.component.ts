import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { PengirimanService } from '../../services/pengiriman.service';

@Component({
  selector: 'app-pengiriman',
  templateUrl: './pengiriman.component.html',
  styleUrls: ['../../../styles.scss']
})
export class PengirimanComponent implements OnInit {

  constructor(
    private api:PengirimanService
  ) { }

  ngOnInit() {
    this.get();
    this.api.test().subscribe(data=>{
      console.log(data);
      },
      error=>{
        console.log(error);
      }
    )
  }

  dataSource:any=[];
  get(){
    this.api.ambil().subscribe(data=>{
      this.dataSource = data;
    })
  }

}
