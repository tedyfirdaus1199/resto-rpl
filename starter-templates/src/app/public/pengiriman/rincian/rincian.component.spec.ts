import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RincianComponent } from './rincian.component';

describe('RincianComponent', () => {
  let component: RincianComponent;
  let fixture: ComponentFixture<RincianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RincianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RincianComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
