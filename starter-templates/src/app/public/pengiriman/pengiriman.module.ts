import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PengirimanComponent } from './pengiriman.component';
import { RincianComponent } from './rincian/rincian.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '',
             component: PengirimanComponent 
            },
            {
            path:'rincian',
            component:RincianComponent
            }
        ])
    ],
    declarations: [

        PengirimanComponent,

        RincianComponent

    ],
})
export class PengirimanModule { }
