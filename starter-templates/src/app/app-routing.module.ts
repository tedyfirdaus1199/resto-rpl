import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicComponent } from './public/public.component';
import { KatalogComponent } from './public/katalog/katalog.component';
import { PesananComponent } from './public/pesanan/pesanan.component';
import { PembayaranComponent } from './public/pembayaran/pembayaran.component';
import { PromosiComponent } from './public/promosi/promosi.component';
import { PengirimanComponent } from './public/pengiriman/pengiriman.component';

const routes: Routes = [
  { path: '', redirectTo: 'public', pathMatch: 'full' },
  { path: 'public', component: PublicComponent },
  { path: 'login', loadChildren: './login/login.module#LoginModule'
  },
  { path: 'katalog', component: KatalogComponent },
  { path: 'pesanan', component: PesananComponent },
  { path: 'pembayaran', component: PembayaranComponent },
  { path: 'promosi', component: PromosiComponent },
  { path: 'pengiriman', component: PengirimanComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
