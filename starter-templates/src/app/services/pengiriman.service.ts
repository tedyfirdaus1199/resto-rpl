import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";


@Injectable({
  providedIn: 'root'
})
export class PengirimanService {

  constructor(
    private http:HttpClient
    ) { }
  
  serverURL:any='http://localhost/resto-api/index.php/';

  test(){
    return this.http.get(this.serverURL+'pengiriman/status');
  }

  ambil(){
    return this.http.get(this.serverURL+'pengiriman/');
  }


  // getPromo(nama_promo: string): Observable<any> {
  //   return this.http.get(`${this.url}/index?nama_promo=${encodeURI(nama_promo)}`).pipe(
  //       map(results => results['promo'])
  //   );
  // }


}
