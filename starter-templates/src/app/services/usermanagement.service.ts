import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsermanagementService {
  
  url='http://localhost/resto-api/index.php/';

  constructor(private http: HttpClient) { 
	
  }

  	getuser(nama: string): Observable<any> {
    return this.http.get(`${this.url}/usermanagement/ambil/index?nama=${encodeURI(nama)}`).pipe(
        map(results => results['user'])
    );
  }
}
