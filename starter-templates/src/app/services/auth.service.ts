import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  url = 'http://localhost/resto-api/index.php/login';
  token;

  constructor(public http: HttpClient, private router: Router) { }

  login(email: string, password: string) {
    this.http.post(this.url + '/login', {email: email,password: password})
    .subscribe((resp: any) => {
     
      this.router.navigate(['dashboard']);
      localStorage.setItem('auth_token', resp.token);
      },
      error => {
          alert('email atau password salah!');
          console.log(error);
      })
  }

  registrasi(nama: string, email: string, password: string) {
    this.http.post(this.url + '/registrasi', {nama: nama, email: email,password: password})
    .subscribe((resp: any) => {
    
      alert('registrasi berhasil!');
      this.router.navigate(['/login']);
      localStorage.setItem('auth_token', resp.token);
      },
      error => {
          alert('registrasi gagal!');
          console.log(error);
      })
  }

  logout() {
    localStorage.removeItem('token');
  }
 
  public get logIn(): boolean {
    return (localStorage.getItem('token') !== null);
  }
}
