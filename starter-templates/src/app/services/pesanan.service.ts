import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PesananService {

  url:any='http://localhost/resto-api/pesanan/';

  constructor(private router:Router,private http:HttpClient) { }
 
  // getPesanan() {
  //   return this.http.get(this.url+'pesanan');
  // }

  getPesanan(kode_pesanan: string): Observable<any> {
    return this.http.get(`${this.url}/pesanan?kode_pesanan=${encodeURI(kode_pesanan)}`).pipe(
        map(results => results['pesanan'])
    );
  }

  tambahdata(kode_pesanan: string, kode_user: string, kode_makanan: string, jumlah_makanan: string, kode_minuman: string, jumlah_minuman: string, subtotal_makanan: string, subtotal_minuman: string, total_bayar: string, kode_promo: string, meja: string) {
    this.http.post(this.url + '/tambah', {kode_pesanan: kode_pesanan, kode_user: kode_user, kode_makanan: kode_makanan, jumlah_makanan: jumlah_makanan, kode_minuman: kode_minuman, jumlah_minuman: jumlah_minuman, subtotal_makanan: subtotal_makanan, subtotal_minuman: subtotal_minuman, total_bayar: total_bayar, kode_promo: kode_promo, meja: meja})
    .subscribe((resp: any) => {
      this.router.navigate(['/dashboard/pesanan']);
      localStorage.setItem('kode_pesanan', kode_pesanan);
      alert('Data berhasil ditambah');
    },
    error => {
      alert('Data gagal ditambah');
      console.log(error);
    }
    )
  }
}
