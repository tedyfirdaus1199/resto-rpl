import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PromoService {
 
  url ='http://localhost/resto-api/index.php/promo';
  token;

  constructor(
  private router: Router,
  private http:HttpClient) { }

  getPromo(nama_promo: string): Observable<any> {
    return this.http.get(`${this.url}/promo/index?nama_promo=${encodeURI(nama_promo)}`).pipe(
        map(results => results['promo'])
    );
  }

  tambahdata(nama_promo: string, kd_promo: string, kategori: string, jumlah_potongan: string, max_penggunaan: string, tgl_selesai: string, keterangan: string, s_dan_k: string) {

  	this.http.post(this.url + '/tambah', {nama_promo:nama_promo, kd_promo:kd_promo, kategori:kategori, jumlah_potongan:jumlah_potongan, max_penggunaan:max_penggunaan, tgl_selesai:tgl_selesai, keterangan:keterangan, s_dan_k:s_dan_k})
  	.subscribe((resp: any)	=> {
  		this.router.navigate(['/dashboard/promo']);
  		localStorage.setItem('auth_token', resp.token);
  		alert('Promo berhasil ditambahkan');
  	},
  	error	=> {
  		alert('Promo gagal ditambahkan!');
  		console.log(error);
  	})
  }
}
