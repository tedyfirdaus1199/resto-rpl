import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboradComponent } from './dashborad/dashborad.component';


const routes: Routes = [
  {
    path: 'dashboard', component: DashboradComponent,
    children: [
      {
        path: 'menu', loadChildren: '../layout/menu/menu.module#MenuModule',
      },
      {
        path: 'usermanagement', loadChildren: '../layout/usermanagement/usermanagement.module#UserModule',
      },
      {
        path: 'pembayaran', loadChildren: '../layout/pembayaran/pembayaran.module#PembayaranModule',
      },
      {
        path: 'pengiriman', loadChildren: '../layout/pengiriman/pengiriman.module#PengirimanModule',
      },
      {
        path: 'laporan', loadChildren: '../layout/laporan/laporan.module#LaporanModule',
      },
      {
        path: 'layanankonsumen', loadChildren: '../layout/layanankonsumen/layanankonsumen.module#LayanankonsumenModule',
      },
      {
        path: 'promo', loadChildren: '../layout/promo/promo.module#PromoModule',
      },
      {
        path: 'pesanan', loadChildren: '../layout/pesanan/pesanan.module#PesananModule',
      },

    ],
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
