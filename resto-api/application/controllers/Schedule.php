<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Schedule extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');        
        date_default_timezone_set('Asia/Jakarta');
        $this->auth();
    }

    function base_get()
    {
        $cid=$this->user_data->uid;
        $start=date_create($this->get('start'));
        $end=date_create($this->get('end'));

        $start->modify('-7 day');
        $s_stamp=date_format($start,'Y-m-d');
       
        $end->modify('+7 day');
        $e_stamp=date_format($end,'Y-m-d');
       

        $start_stamp=strtotime($s_stamp);
        $end_stamp=strtotime($e_stamp);

        /*
        $this->db->where('start_date >=',$start);
        $this->db->where('end_date <=',$end);
        $this->db->where('cid',$cid);
        $q=$this->db->get('schedule');
        */

        $q=$this->db->query("SELECT * FROM schedule WHERE 
           
            ((UNIX_TIMESTAMP(end_date) <= $end_stamp) AND
            (UNIX_TIMESTAMP(start_date) >= $start_stamp)) AND

            cid='$cid'");
        $res=array();
        if($q->num_rows()>0)
        {
            foreach($q->result() as $row)
            {
                $t1=new DateTime($row->start_date.' '.$row->start_time);
                $t2=new DateTime($row->start_date.' 24:00');
                $start_width=$t1->diff($t2)->h;

                $e1=new DateTime($row->end_date.' '.$row->end_time);
                $e2=new DateTime($row->end_date.' 24:00');
                $end_width=$e1->diff($e2)->h;


                $row->start_width=$start_width;
                $row->end_width=24-$end_width;
                $res[]=$row;
            }
        }
        $this->response($res,200);
    }

    function param_put()
    {
        $id=$this->uri->segment(2);
        $data=$this->put();
        $this->db->where('id',$id);
        $res=$this->db->update('schedule',$data);
        $this->response($res,200);
    }

    function param_delete()
    {
        $id=$this->uri->segment(2);        
        $this->db->where('id',$id);
        $res=$this->db->delete('schedule');
        $this->response($res,200);
    }

}