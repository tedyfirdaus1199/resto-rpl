<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Pesanan extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');
        $this->load->model('M_pesanan');
        date_default_timezone_set('Asia/Jakarta');
        // $this->auth();


        
        $this->load->database();
        // $this->load->model('master/data');
        // $this->load->model('m_pesanan');
    }

    function pesanan_get()
    {
        // tampil sesuai id tetapi blm bisa tampil semua hanya 1 output
        // $kode_pesanan = $this->get('kode_pesanan');
        // if ($kode_pesanan === null) {
        //     $this->db->select('*');
        //     $this->db->from('tb_pesanan');
        //     $this->db->join('tb_user','tb_user.kode_user=tb_pesanan.kode_user');

        //     $data = $this->db->get();    
        // } else {

        //     $data = $this->db->get_where('tb_pesanan', ['kode_pesanan' => $kode_pesanan]);

        // }




        //tampil keseluruhan
            $this->db->select('*');
            $this->db->from('tb_pesanan');
            $this->db->join('tb_user','tb_user.kode_user=tb_pesanan.kode_user');

            $data = $this->db->get();    

        return $this->response(['pesanan' => ($data->result())]);

    }

    function pesanan_post()
    {
        // Get the post data
        $kode_pesanan = strip_tags($this->post('kode_pesanan'));
        $kode_user = strip_tags($this->post('kode_user'));
        $kode_makanan = strip_tags($this->post('kode_makanan'));
        $jumlah_makanan = strip_tags($this->post('jumlah_makanan'));
        $kode_minuman = strip_tags($this->post('kode_minuman'));
        $jumlah_minuman = strip_tags($this->post('jumlah_minuman'));
        $subtotal_makanan = strip_tags($this->post('subtotal_makanan'));
        $subtotal_minuman = strip_tags($this->post('subtotal_minuman'));
        $total_bayar = strip_tags($this->post('total_bayar'));
        $kode_promo = strip_tags($this->post('kode_promo'));
        $meja = strip_tags($this->post('meja'));
        
        // Validate the post data
        if(!empty($kode_pesanan) && !empty($kode_user) && !empty($kode_makanan) && !empty($jumlah_makanan) && !empty($kode_minuman) && !empty($jumlah_minuman) && !empty($subtotal_makanan) && !empty($subtotal_minuman) && !empty($total_bayar) && !empty($kode_promo) && !empty($meja)){
            
            // Check if the given already exists
            $con['returnType'] = 'count';
            $con['conditions'] = array(
                'kode_pesanan' => $kode_pesanan,
                'kode_user'    => $kode_user
            );
            $pesananCount = $this->M_pesanan->getRows($con);
            
            if($pesananCount > 0){
                // Set the response and exit
                $this->response("pesanan sudah tersedia.", REST_Controller::HTTP_BAD_REQUEST);
            }else{
                // Insert user data
                $pesananData = array(
                    'kode_pesanan' => $kode_pesanan,
                    'kode_user' => $kode_user,
                    'kode_makanan' => $kode_makanan,
                    'jumlah_makanan' => $jumlah_makanan,
                    'kode_minuman' => $kode_minuman,
                    'jumlah_minuman' => $jumlah_minuman,
                    'subtotal_makanan' => $subtotal_makanan,
                    'subtotal_minuman' => $subtotal_minuman,
                    'total_bayar' => $total_bayar,
                    'kode_promo' => $kode_promo,
                    'meja' => $meja,
                );
                $insert = $this->M_pesanan->insert($pesananData);
                
                // Check if the user data is inserted
                if($insert){
                    // Set the response and exit
                    $this->response([
                        'status' => TRUE,
                        'message' => 'Pesanan has been added successfully.',
                        'data' => $insert
                    ], REST_Controller::HTTP_OK);
                }else{
                    // Set the response and exit
                    $this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }else{
            // Set the response and exit
            $this->response("Provide complete user info to add.", REST_Controller::HTTP_BAD_REQUEST);
        }
    }

}