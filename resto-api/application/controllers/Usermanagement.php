<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Usermanagement extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');        
        date_default_timezone_set('Asia/Jakarta');
        //$this->auth();
    }

    function ambil_get()
    {
        //$cid=$this->user_data->uid;
        $res=$this->db->get_where('tb_user_management')->result();
        $this->response(['user' => $res],200);
    }

    function user_post()
    {
        $data=$this->post();
        //$data['cid']=$this->user_data->uid;
        $data['id_pengguna']=uniqid();
        $res=$this->db->insert('tb_user_management',$data);
        $this->response(['user' => $res],200);
    }

}