<?php
class Upload extends CI_Controller {
    function __construct()
    {
         parent::__construct();
         header("Access-Control-Allow-Origin: *");
         header("Access-Control-Allow-Headers: Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
		 header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $this->load->helper(array('form', 'url'));
        $this->load->model('M_main');
        date_default_timezone_set('Asia/Jakarta');
        //$this->auth();
        
    }
    
    function status()
    {
        $mail=$this->M_main->sendmail('farid.suryanto@is.uad.ac.id','TEST SEND MAIL','content of mail');
        echo json_encode($mail);
    }
    
    
    
    
    function image()
    {
        // Check if image file is a actual image or fake image
        if(isset($_FILES["file"])) {
            $target_dir = "uploads/images/";
            $fileRename=uniqid().'_'. basename($_FILES["file"]["name"]);
            $target_file = $target_dir .$fileRename;
            $uploadOk = 1;
            $dt=false;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            //$mime=mime_content_type($target_file);
            $check = getimagesize($_FILES["file"]["tmp_name"]);
            if($check !== false) {
                //cek jenis file yang diijinkan
                if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
                    $msg= "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                    $uploadOk = 0;
                }else{
                    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                        //catat di database upload_file
                        $dat=array('name'=>$fileRename,'size'=>filesize($target_file),'url'=>$target_file,'provider'=>'local','ext'=>$imageFileType);
                        $dt=$this->M_main->create('upload_file',$dat,null);
                        $msg='Upload success';
                    } else {
                        $msg= "Sorry, there was an error uploading your file.";
                        $uploadOk=0;
                    }
                }
            } else {
                $msg= "File is not an image.";
                $uploadOk = 0;
            }
        }else{
            $msg='No file uploaded';
        }
        
        $res=array('status'=>$uploadOk,'message'=>$msg,'data'=>$dt,'base_url'=>base_url());
        echo json_encode($res);
        
    }
    
    function file()
    {
        // Check if image file is a actual image or fake image
        if(isset($_FILES["file"])) {
            $target_dir = "uploads/files/";
            $fileRename=uniqid().'_'. basename($_FILES["file"]["name"]);
            $target_file = $target_dir .$fileRename;
            $uploadOk = 1;
            $dt=false;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            //$mime=mime_content_type($target_file);
                //cek jenis file yang diijinkan
                if($imageFileType != "pdf" && $imageFileType != "doc" && $imageFileType != "docx" && $imageFileType != "xls" && $imageFileType != "xlsx") {
                    $msg= "Maaf, hanya file PDF yang dapat di unggah.";
                    $uploadOk = 0;
                }else{
                    if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                        //catat di database upload_file
                        $dat=array('name'=>$fileRename,'size'=>filesize($target_file),'url'=>$target_file,'provider'=>'local','ext'=>$imageFileType);
                        $dt=$this->M_main->create('upload_file',$dat,null);
                        $msg='Upload success';
                    } else {
                        $msg= "Sorry, there was an error uploading your file.";
                        $uploadOk=0;
                    }
                }
           
        }else{
            $msg='No file uploaded';
        }
        
        $res=array('status'=>$uploadOk,'message'=>$msg,'data'=>$dt,'base_url'=>base_url());
        echo json_encode($res);
        
    }
    
    function destroyfile()
    {
        $url=$_POST['url'];
        if(file_exists($url))
        unlink($url);
        $this->db->where('url',$url);
        $this->db->delete('upload_file');
        echo true;
    }
    
}