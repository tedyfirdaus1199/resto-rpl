<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Bus extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');        
        date_default_timezone_set('Asia/Jakarta');
        $this->auth();
    }

    function base_get()
    {
        $cid=$this->user_data->uid;
        $res=$this->db->get_where('bus',array('cid'=>$cid))->result();
        $this->response($res,200);
    }

    function base_post()
    {
        $data=$this->post();
        $data['cid']=$this->user_data->uid;
        $data['bid']=uniqid();
        $res=$this->db->insert('bus',$data);
        $this->response($res,200);
    }

}