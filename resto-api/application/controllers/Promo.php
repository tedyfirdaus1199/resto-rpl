<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Promo extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');  
        $this->load->model('M_promo');        
        $this->load->database();
        date_default_timezone_set('Asia/Jakarta');
        //$this->auth();
    }

    function promo_get()
    {
        //$cid=$this->user_data->uid;
        $res=$this->db->get_where('tb_promo')->result();
        $this->response(['promo' => $res],200);
    }

    function tambah_post()
    {
        // Get the post data
        $nama_promo = strip_tags($this->post('nama_promo'));
        $kd_promo = strip_tags($this->post('kd_promo'));
        $kategori = strip_tags($this->post('kategori'));
        $jumlah_potongan = strip_tags($this->post('jumlah_potongan'));
        $max_penggunaan = strip_tags($this->post('max_penggunaan'));
        $tgl_selesai = strip_tags($this->post('tgl_selesai'));
        $keterangan = strip_tags($this->post('keterangan'));
        $s_dan_k = strip_tags($this->post('s_dan_k'));
        
        // Validate the post data
        if(!empty($nama_promo) && !empty($kd_promo) && !empty($kategori) && !empty($jumlah_potongan) && !empty($max_penggunaan) && !empty($tgl_selesai) && !empty($keterangan) && !empty($s_dan_k)){
            
            // Check if the given nama/kode promo already exists
            $con['returnType'] = 'count';
            $con['conditions'] = array(
                'nama_promo'    => $nama_promo,
                'kd_promo'      => $kd_promo
            );
            $promoCount = $this->M_promo->getRows($con);
            
            if($promoCount > 0){
                // Set the response and exit
                $this->response("Nama / kode promo sudah tersedia!.", REST_Controller::HTTP_BAD_REQUEST);
            }else{
                // Insert promo data
                $promoData = array(
                    'nama_promo' => $nama_promo,
                    'kd_promo' => $kd_promo,
                    'kategori' => $kategori,
                    'jumlah_potongan' => $jumlah_potongan,
                    'max_penggunaan' => $max_penggunaan,
                    'tgl_selesai' => $tgl_selesai,
                    'keterangan' => $keterangan,
                    's_dan_k' => $s_dan_k
                );
                $insert = $this->M_promo->insert($promoData);
                
                // Check if the promo data is inserted
                if($insert){
                    // Set the response and exit
                    $this->response([
                        'status' => TRUE,
                        'message' => 'The promo has been added successfully.',
                        'data' => $insert
                    ], REST_Controller::HTTP_OK);
                }else{
                    // Set the response and exit
                    $this->response("Some problems occurred, please try again.", REST_Controller::HTTP_BAD_REQUEST);
                }
            }
        }else{
            // Set the response and exit
            $this->response("Provide complete promo info to add.", REST_Controller::HTTP_BAD_REQUEST);
        }
    }
}