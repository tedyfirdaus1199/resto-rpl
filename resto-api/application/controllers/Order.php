<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use \Firebase\JWT\JWT;

class Order extends BD_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key
        $this->load->model('M_main');        
        date_default_timezone_set('Asia/Jakarta');
        $this->auth();
    }

    function base_post()
    {
        $data=$this->post();
        $uid=$this->user_data->uid;
        $role=$this->user_data->role;
        $order_status='pending';
        if($role=='company')
        {
            $data['cid']=$uid;
            $order_status=$data['status'];
        }
        $data['oid']=uniqid();
        //order data
        $orderData=array(
            'customer_name'=>$data['customer_name'],
            'customer_phone'=>$data['customer_phone'],
            'oid'=>$data['oid'],
            'cid'=>$data['cid'],
            'pickup'=>$data['pickup'],
            'destination'=>$data['destination'],
            'note'=>$data['note'],
            'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'],
            'start_time'=>$data['start_time'],
            'end_time'=>$data['end_time'],
            'status'=>$order_status
        );
        $res=$this->db->insert('orders',$orderData);

        //jadwal data
        $scheduleData=array(
            'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'],
            'start_time'=>$data['start_time'],
            'end_time'=>$data['end_time'],
            'oid'=>$data['oid'],
            'cid'=>$data['cid'],
            'status'=>$order_status
        );

        //buat jadwal jika posting dari company
        if($role=='company')
        $this->setjadwal($scheduleData,$data['bus']);
        $this->response($res,200);
    }

    function param_get()
    {
        $oid=$this->uri->segment(2);
        $dt=array('oid'=>$oid);
        $q=$this->db->get_where('orders',$dt);
        $this->response($q->row(),200);
    }

    function param_put()
    {
        $oid=$this->uri->segment(2);
        $data=$this->put();
        $uid=$this->user_data->uid;
        $role=$this->user_data->role;
        $order_status='pending';
        if($role=='company')
        {
            $data['cid']=$uid;
            $order_status=$data['status'];
        }

        //convert date format
        $start=date_create($data['start_date']);        
        $end=date_create($data['end_date']);

        
        //order data
        $orderData=array(
            'customer_name'=>$data['customer_name'],
            'customer_phone'=>$data['customer_phone'],
            'oid'=>$data['oid'],
            'cid'=>$data['cid'],
            'pickup'=>$data['pickup'],
            'destination'=>$data['destination'],
            'note'=>$data['note'],
            'start_date'=>date_format($start,'Y-m-d'),
            'end_date'=>date_format($end,'Y-m-d'),
            'start_time'=>$data['start_time'],
            'end_time'=>$data['end_time'],
            'status'=>$order_status
        );
        $this->db->where('oid',$oid);
        $res=$this->db->update('orders',$orderData);

        //jadwal data
        $scheduleData=array(
            'start_date'=>$data['start_date'],
            'end_date'=>$data['end_date'],
            'start_time'=>$data['start_time'],
            'end_time'=>$data['end_time'],            
            'status'=>$order_status
        );

        //buat jadwal jika posting dari company
        if($role=='company')
        $this->updatejadwal($scheduleData,$data['bus'],$oid);
        $this->response($res,200);

    }

    function setjadwal($data,$bus)
    {
        foreach($bus as $row)
        {
            $data['bid']=$row;
            $this->db->insert('schedule',$data);
        }
        return;
    }

    function updatejadwal($data,$bus,$oid)
    {
        foreach($bus as $row)
        {
            $this->db->where('oid',$oid);
            $this->db->update('schedule',$data);
        }
        return;
    }


}