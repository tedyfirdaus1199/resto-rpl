<?php if(!defined('BASEPATH')) exit('no direct script allowed');
use \Firebase\JWT\JWT;

class Pengiriman extends BD_Controller {

    function __construct(){

        parent::__construct();

        header("Access-Control-Allow-Origin: *");
        $this->methods['users_get']['limit'] = 500;
        $this->methods['users_post']['limit'] = 100;
        $this->methods['users_delete']['limit'] = 50;
        $this->load->model('M_pengiriman');
        date_default_timezone_set('Asia/Jakarta');
        
    }

    public function index_get()
    {
        $id = $this->get('id_pesanan');
        $kirim = $this->M_pengiriman->get_kirim($id);

        echo json_encode($kirim);
    }

    function status_get(){
        $this->response('berhasil',200);
    }
}