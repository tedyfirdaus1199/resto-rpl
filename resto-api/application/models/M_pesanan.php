<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_pesanan extends CI_Model{

    // public function get_join($select,$table,$kolom=null,$order_by=null)

    // {
    //     if($order_by){
    //         $this->db->order_by($kolom,$order_by);
    //     }
    //     $this->db->select($select);
    //     // $this->db->join('tb_makanan mk','ps.id_guru=g.id');
    //     // $this->db->join('tb_minuman mi','gj.id_jabatan=j.id');
    //     $this->db->join('tb_detail_makanan dmk','ps.kode_pesanan=dmk.kode_pesanan');
    //     $this->db->join('tb_detail_minuman dmi','ps.kode_pesanan=dmi.kode_pesanan');
    //     $this->db->join('tb_user u','ps.kode_user=u.kode_user');
        

    //     return $this->db->get($table);
    // }

    function getRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->tb_pesanan);

        if(array_key_exists("conditions", $params)){
            foreach ($params as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        if(array_key_exists("kode_pesanan", $params)){
            $this->db->where('kode_pesanan',$params['kode_pesanan']);
            $query = $this->db->get();
            $result = $query->row_array();
        } else {
            if(array_key_exists("start", $params) && array_key_exists("limit", $params)){
                $this->db->limit($params['limit'],$params['start']);
            } elseif(!array_key_exists("start", $params) && array_key_exists("limit", $params)){
                $this->db->limit($params['limit']);
            }

            if(array_key_exists("returnType", $params) && $params['returnType'] == 'count'){
                $result = $this->db->count_all_result();
            } elseif(!array_key_exists("returnType", $params) && $params['returnType'] == 'count'){
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->row_array():false;
            } else{
                $query = $this->db->get();
                $result = ($query->num_rows() > 0)?$query->result_array():false;
            }
        }

        return $result;

    }

    public function insert($data){
        //add created and modified date if not exists
        if(!array_key_exists("created", $data)){
            $data['created'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("modified", $data)){
            $data['modified'] = date("Y-m-d H:i:s");
        }
        
        //insert user data to users table
        $insert = $this->db->insert($this->tb_pesanan, $data);
        
        //return the status
        return $insert?$this->db->insert_id():false;
    }
}
