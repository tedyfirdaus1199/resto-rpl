<?php if(!defined('BASEPATH')) exit('No direct script allowed');

class M_pengiriman extends CI_Model{

    function get_kirim ($id = null)
    {
        if($id === null)
        {
            return $this->db->get('tb_pengiriman')->result_array();
        }else{
            return $this->db->get_where('tb_pengiriman',['id'=> $id])->result_array();
        }
    }
}