/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.39-MariaDB : Database - new_resto
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`new_resto` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `new_resto`;

/*Table structure for table `tb_detail_makanan` */

DROP TABLE IF EXISTS `tb_detail_makanan`;

CREATE TABLE `tb_detail_makanan` (
  `kode_pesanan` int(20) NOT NULL,
  `kode_makanan` int(20) NOT NULL,
  `jumlah_makanan` int(100) NOT NULL,
  `total_makanan` int(100) NOT NULL,
  `ket_makanan` text NOT NULL,
  PRIMARY KEY (`kode_pesanan`,`kode_makanan`),
  KEY `kode_makanan` (`kode_makanan`),
  CONSTRAINT `tb_detail_makanan_ibfk_1` FOREIGN KEY (`kode_makanan`) REFERENCES `tb_makanan` (`kode_makanan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_detail_makanan` */

insert  into `tb_detail_makanan`(`kode_pesanan`,`kode_makanan`,`jumlah_makanan`,`total_makanan`,`ket_makanan`) values 
(0,0,2,30000,''),
(1,1,1,15000,'');

/*Table structure for table `tb_detail_minuman` */

DROP TABLE IF EXISTS `tb_detail_minuman`;

CREATE TABLE `tb_detail_minuman` (
  `kode_pesanan` int(20) NOT NULL,
  `kode_minuman` int(20) NOT NULL,
  `jumlah_minuman` int(100) NOT NULL,
  `total_minuman` int(100) NOT NULL,
  `ket_minuman` text NOT NULL,
  PRIMARY KEY (`kode_pesanan`,`kode_minuman`),
  KEY `kode_minuman` (`kode_minuman`),
  CONSTRAINT `tb_detail_minuman_ibfk_1` FOREIGN KEY (`kode_minuman`) REFERENCES `tb_minuman` (`kode_minuman`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_detail_minuman` */

insert  into `tb_detail_minuman`(`kode_pesanan`,`kode_minuman`,`jumlah_minuman`,`total_minuman`,`ket_minuman`) values 
(0,0,2,10000,''),
(1,0,1,5000,'');

/*Table structure for table `tb_kategori` */

DROP TABLE IF EXISTS `tb_kategori`;

CREATE TABLE `tb_kategori` (
  `kode_kategori` int(20) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL,
  PRIMARY KEY (`kode_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_kategori` */

insert  into `tb_kategori`(`kode_kategori`,`nama_kategori`) values 
(1,'makanan'),
(2,'minuman');

/*Table structure for table `tb_level_user` */

DROP TABLE IF EXISTS `tb_level_user`;

CREATE TABLE `tb_level_user` (
  `kode_level_user` int(20) NOT NULL,
  `nama_level_user` varchar(255) NOT NULL,
  PRIMARY KEY (`kode_level_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_level_user` */

insert  into `tb_level_user`(`kode_level_user`,`nama_level_user`) values 
(1,'admin'),
(2,'owner'),
(3,'kasir'),
(4,'member'),
(5,'public'),
(6,'waitress');

/*Table structure for table `tb_makanan` */

DROP TABLE IF EXISTS `tb_makanan`;

CREATE TABLE `tb_makanan` (
  `kode_makanan` int(20) NOT NULL,
  `nama_makanan` varchar(255) NOT NULL,
  `gambar_makanan` text NOT NULL,
  `stok_makanan` int(20) NOT NULL,
  `harga_makanan` int(100) NOT NULL,
  `deskripsi_makanan` text NOT NULL,
  `kode_kategori` int(20) NOT NULL,
  PRIMARY KEY (`kode_makanan`),
  KEY `kode_kategori` (`kode_kategori`),
  CONSTRAINT `tb_makanan_ibfk_1` FOREIGN KEY (`kode_kategori`) REFERENCES `tb_kategori` (`kode_kategori`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_makanan` */

insert  into `tb_makanan`(`kode_makanan`,`nama_makanan`,`gambar_makanan`,`stok_makanan`,`harga_makanan`,`deskripsi_makanan`,`kode_kategori`) values 
(0,'nasi goreng','',5,15000,'',1),
(1,'geprek','',5,12000,'',1);

/*Table structure for table `tb_minuman` */

DROP TABLE IF EXISTS `tb_minuman`;

CREATE TABLE `tb_minuman` (
  `kode_minuman` int(20) NOT NULL,
  `nama_minuman` varchar(255) NOT NULL,
  `gambar_minuman` text NOT NULL,
  `stok_minuman` int(20) NOT NULL,
  `harga_minuman` int(100) NOT NULL,
  `deskripsi_minuman` text NOT NULL,
  `kode_kategori` int(20) NOT NULL,
  PRIMARY KEY (`kode_minuman`),
  KEY `kode_kategori` (`kode_kategori`),
  CONSTRAINT `tb_minuman_ibfk_1` FOREIGN KEY (`kode_kategori`) REFERENCES `tb_kategori` (`kode_kategori`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_minuman` */

insert  into `tb_minuman`(`kode_minuman`,`nama_minuman`,`gambar_minuman`,`stok_minuman`,`harga_minuman`,`deskripsi_minuman`,`kode_kategori`) values 
(0,'es teh','',5,5000,'',2),
(1,'es jeruk','',5,5000,'',2);

/*Table structure for table `tb_pesanan` */

DROP TABLE IF EXISTS `tb_pesanan`;

CREATE TABLE `tb_pesanan` (
  `kode_pesanan` int(20) NOT NULL,
  `tgl_pesanan` date NOT NULL,
  `kode_user` int(20) NOT NULL,
  `subtotal_makanan` int(50) NOT NULL,
  `subtotal_minuman` int(50) NOT NULL,
  `total_bayar` int(100) NOT NULL,
  `kode_promo` int(20) NOT NULL,
  `meja` char(100) NOT NULL,
  PRIMARY KEY (`kode_pesanan`),
  KEY `kode_user` (`kode_user`),
  KEY `kode_promo` (`kode_promo`),
  CONSTRAINT `tb_pesanan_ibfk_1` FOREIGN KEY (`kode_user`) REFERENCES `tb_user` (`kode_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_pesanan_ibfk_2` FOREIGN KEY (`kode_promo`) REFERENCES `tb_promo` (`kode_promo`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_pesanan_ibfk_3` FOREIGN KEY (`kode_pesanan`) REFERENCES `tb_detail_makanan` (`kode_pesanan`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `tb_pesanan_ibfk_4` FOREIGN KEY (`kode_pesanan`) REFERENCES `tb_detail_minuman` (`kode_pesanan`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_pesanan` */

insert  into `tb_pesanan`(`kode_pesanan`,`tgl_pesanan`,`kode_user`,`subtotal_makanan`,`subtotal_minuman`,`total_bayar`,`kode_promo`,`meja`) values 
(0,'2019-12-25',0,30000,10000,40000,0,'20'),
(1,'2019-12-25',1,15000,5000,20000,0,'10');

/*Table structure for table `tb_promo` */

DROP TABLE IF EXISTS `tb_promo`;

CREATE TABLE `tb_promo` (
  `kode_promo` int(20) NOT NULL,
  `nama_promo` varchar(255) NOT NULL,
  `kategori` int(20) NOT NULL,
  `tgl_mulai` date NOT NULL,
  `tgl_selesai` date NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`kode_promo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_promo` */

insert  into `tb_promo`(`kode_promo`,`nama_promo`,`kategori`,`tgl_mulai`,`tgl_selesai`,`keterangan`) values 
(0,'grabwae',1,'2019-12-24','2019-12-24','');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `kode_user` int(20) NOT NULL,
  `kode_level_user` int(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `no_hp` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`kode_user`),
  KEY `kode_level_user` (`kode_level_user`),
  CONSTRAINT `tb_user_ibfk_1` FOREIGN KEY (`kode_level_user`) REFERENCES `tb_level_user` (`kode_level_user`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_user` */

insert  into `tb_user`(`kode_user`,`kode_level_user`,`username`,`password`,`no_hp`,`alamat`,`email`) values 
(0,5,'miko','aaaa','08123281','jogja','a@gmail.com'),
(1,5,'bangun','bbb','08888888','bantul','b@gmail.com');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
