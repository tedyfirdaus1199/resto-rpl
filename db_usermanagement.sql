/*
SQLyog Professional v12.4.3 (64 bit)
MySQL - 10.1.36-MariaDB : Database - database_resto
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`database_resto` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `database_resto`;

/*Table structure for table `tb_datauser` */

DROP TABLE IF EXISTS `tb_datauser`;

CREATE TABLE `tb_datauser` (
  `nama_pengguna` varchar(255) NOT NULL,
  `no_hp` int(15) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `id_pengguna` int(255) NOT NULL AUTO_INCREMENT,
  `id_pesanan` int(10) NOT NULL,
  UNIQUE KEY `id_pengguna` (`id_pengguna`),
  KEY `id_pesanan` (`id_pesanan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_datauser` */

/*Table structure for table `tb_layanan_konsumen` */

DROP TABLE IF EXISTS `tb_layanan_konsumen`;

CREATE TABLE `tb_layanan_konsumen` (
  `nama` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `perihal` varchar(100) NOT NULL,
  `pesan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_layanan_konsumen` */

/*Table structure for table `tb_makanan` */

DROP TABLE IF EXISTS `tb_makanan`;

CREATE TABLE `tb_makanan` (
  `kode_makanan` int(10) NOT NULL,
  `nama_makanan` varchar(100) NOT NULL,
  `harga_makanan` varchar(100) NOT NULL,
  `gambar_makanan` varchar(10) NOT NULL,
  `deskripsi_makanan` varchar(255) NOT NULL,
  KEY `kode_makanan` (`kode_makanan`),
  CONSTRAINT `tb_makanan_ibfk_1` FOREIGN KEY (`kode_makanan`) REFERENCES `tb_pesanan` (`kode_makanan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_makanan` */

/*Table structure for table `tb_minum` */

DROP TABLE IF EXISTS `tb_minum`;

CREATE TABLE `tb_minum` (
  `kode_minuman` int(10) NOT NULL,
  `nama_minuman` varchar(10) NOT NULL,
  `harga_minuman` varchar(10) NOT NULL,
  `gambar_minuman` varchar(100) NOT NULL,
  `deskripsi_minuman` varchar(100) NOT NULL,
  KEY `kode_minuman` (`kode_minuman`),
  CONSTRAINT `tb_minum_ibfk_1` FOREIGN KEY (`kode_minuman`) REFERENCES `tb_pesanan` (`kode_minuman`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_minum` */

/*Table structure for table `tb_pesanan` */

DROP TABLE IF EXISTS `tb_pesanan`;

CREATE TABLE `tb_pesanan` (
  `kode_makanan` int(10) NOT NULL,
  `kode_minuman` int(10) NOT NULL,
  `id_pesanan` int(10) NOT NULL,
  `kd_promo` varchar(10) NOT NULL,
  KEY `kode_makanan` (`kode_makanan`),
  KEY `kode_minuman` (`kode_minuman`),
  KEY `id_pesanan` (`id_pesanan`),
  KEY `kd_promo` (`kd_promo`),
  CONSTRAINT `tb_pesanan_ibfk_3` FOREIGN KEY (`id_pesanan`) REFERENCES `tb_datauser` (`id_pesanan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_pesanan` */

/*Table structure for table `tb_promo` */

DROP TABLE IF EXISTS `tb_promo`;

CREATE TABLE `tb_promo` (
  `kd_promo` varchar(10) NOT NULL,
  `nama_promo` varchar(100) NOT NULL,
  `kategori` varchar(10) NOT NULL,
  `tgl_mulai` varchar(10) NOT NULL,
  `tgl_selesai` varchar(10) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `gambar` varchar(10) NOT NULL,
  `s_dan_k` varchar(255) NOT NULL,
  KEY `kd_promo` (`kd_promo`),
  CONSTRAINT `tb_promo_ibfk_1` FOREIGN KEY (`kd_promo`) REFERENCES `tb_pesanan` (`kd_promo`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_promo` */

/*Table structure for table `tb_regis` */

DROP TABLE IF EXISTS `tb_regis`;

CREATE TABLE `tb_regis` (
  `id_pengguna` int(255) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_pengguna` varchar(255) NOT NULL,
  `role_id` int(1) NOT NULL,
  `tanggal_buat` int(11) NOT NULL,
  PRIMARY KEY (`id_pengguna`),
  CONSTRAINT `tb_regis_ibfk_1` FOREIGN KEY (`id_pengguna`) REFERENCES `tb_datauser` (`id_pengguna`),
  CONSTRAINT `tb_regis_ibfk_2` FOREIGN KEY (`id_pengguna`) REFERENCES `tb_user_management` (`id_pengguna`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_regis` */

/*Table structure for table `tb_user_management` */

DROP TABLE IF EXISTS `tb_user_management`;

CREATE TABLE `tb_user_management` (
  `role_id` int(11) NOT NULL,
  `menu_access` varchar(10) NOT NULL,
  `id_pengguna` int(255) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  UNIQUE KEY `id_pengguna` (`id_pengguna`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `tb_user_management` */

insert  into `tb_user_management`(`role_id`,`menu_access`,`id_pengguna`,`nama`,`email`,`password`) values 
(1,'',1,'nita','nita@gmail.com',''),
(0,'',2,'Neni','neni@gmail.com','');

/*Table structure for table `tb_user_role` */

DROP TABLE IF EXISTS `tb_user_role`;

CREATE TABLE `tb_user_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_user_role` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
